FROM rocker/r2u:jammy
LABEL org.opencontainers.image.authors="Aurelien Ginolhac <aurelien.ginolhac@uni.lu>"

# build with: docker buildx build -t ginolhac/snake-atac-seq:0.3 .
# docker push ginolhac/snake-atac-seq:0.3

LABEL version="0.3"
LABEL description="Docker image to build the snakemake atac-seq singularity"

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y software-properties-common && \
    apt-get update && apt-get install -y \
        build-essential \
        cmake \
        curl \
        libboost-all-dev \
        libbz2-dev \
        libcurl4-openssl-dev \
        liblzma-dev \
        libncurses5-dev \
        libssl-dev \
        libxml2-dev \
        libgd-perl \
        locales locales-all \
        openjdk-8-jdk \
        python3 \
        python3-pip \
        unzip \
        vim-common \
        wget \
        zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*


# default to python3
# better to use https://stackoverflow.com/a/50331137/1395352
RUN ln -sf /usr/bin/python-config /usr/bin/python3.8-config && ln -sf /usr/bin/python3 /usr/bin/python


# htslib
ARG HTS_VERSION="1.19.1"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/samtools/htslib/releases/download/${HTS_VERSION}/htslib-${HTS_VERSION}.tar.bz2 && \
    tar -xf htslib-${HTS_VERSION}.tar.bz2 && rm htslib-${HTS_VERSION}.tar.bz2 && cd htslib-${HTS_VERSION} && \
    ./configure --enable-libcurl --enable-s3 --enable-plugins --enable-gcs && \
    make && make install && make clean

# samtools
ARG SAM_VERSION="1.19.2"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/samtools/samtools/releases/download/${SAM_VERSION}/samtools-${SAM_VERSION}.tar.bz2 && \
    tar -xf samtools-${SAM_VERSION}.tar.bz2 && rm samtools-${SAM_VERSION}.tar.bz2 && cd samtools-${SAM_VERSION} && \
    ./configure --with-htslib=/opt/htslib-${HTS_VERSION} && make && make install && make clean

# AdapterRemoval
ARG AR_VERSION="2.3.3"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/MikkelSchubert/adapterremoval/archive/v${AR_VERSION}.tar.gz && \
    tar -xf v${AR_VERSION}.tar.gz && rm v${AR_VERSION}.tar.gz && \
    cd adapterremoval-${AR_VERSION} && make && make install && make clean

# Genrich
ARG GENRICH_VERSION="0.6.1"
RUN cd /opt && \
    wget --no-check-certificate https://github.com/jsh58/Genrich/archive/refs/tags/v${GENRICH_VERSION}.tar.gz && \
    tar -xf v${GENRICH_VERSION}.tar.gz && rm v${GENRICH_VERSION}.tar.gz && \
    cd Genrich-${GENRICH_VERSION} && make && mv Genrich /usr/local/bin/

# FastqScreen
ARG FQS_VERSION="0.15.3"
ENV PERL_MM_USE_DEFAULT=1
RUN cd /opt && \
    wget --no-check-certificate https://github.com/StevenWingett/FastQ-Screen/archive/refs/tags/v${FQS_VERSION}.tar.gz && \
    tar -xf v${FQS_VERSION}.tar.gz && rm v${FQS_VERSION}.tar.gz && perl -MCPAN -e 'install GD::Graph'
ENV PATH /opt/FastQ-Screen-${FQS_VERSION}:$PATH

# fastqc
ARG FQC_VERSION="0.12.1"
RUN cd /opt && \
    wget --no-check-certificate -q https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v${FQC_VERSION}.zip && \
    unzip fastqc_v${FQC_VERSION}.zip && rm fastqc_v${FQC_VERSION}.zip && chmod +x FastQC/fastqc
ENV PATH /opt/FastQC:$PATH

# Bowtie2
ARG BT2_VERSION="2.5.3"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/BenLangmead/bowtie2/releases/download/v${BT2_VERSION}/bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    unzip bowtie2-${BT2_VERSION}-linux-x86_64.zip && rm bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    mv bowtie2-${BT2_VERSION}-linux-x86_64/bowtie2* /usr/local/bin/ && rm -rf bowtie2-${BT2_VERSION}-linux-x86_64

# Picard tools
ARG PIC_VERSION="2.27.5"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/broadinstitute/picard/releases/download/${PIC_VERSION}/picard.jar && \
    wget --no-check-certificate -q -O /usr/local/bin/picard https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-chip-seq/-/raw/main/workflow/scripts/picard && \
    chmod +x /usr/local/bin/picard

RUN mkdir /opt/ucsc && \
    wget --no-check-certificate -q -P /opt/ucsc/ http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bigWigToBedGraph && \
    wget --no-check-certificate -q -P /opt/ucsc/ http://hgdownload.soe.ucsc.edu/admin/exe/linux.x86_64/bedGraphToBigWig && \
    chmod 755 /opt/ucsc/*
ENV PATH /opt/ucsc:$PATH

# bedtools
ARG BT_VERSION="2.31.1"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/arq5x/bedtools2/releases/download/v${BT_VERSION}/bedtools-${BT_VERSION}.tar.gz && \
    tar -xf bedtools-${BT_VERSION}.tar.gz && rm bedtools-${BT_VERSION}.tar.gz && \
    cd bedtools2 && make && make install && make clean

# python modules
RUN python3 -m pip install snakemake snakemake-wrapper-utils  \
  multiqc pandas pyBigWig pysam deeptools spiker tobias macs2 uropa

# R packages
RUN install.r UpsetR corrplot intervene BiocManager && \
  installBioc.r DiffBind csaw


RUN export LC_ALL=en_US.UTF-8 && export LANG=en_US.UTF-8 && locale-gen en_US.UTF-8

# clean up
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    apt-get autoclean && \
    apt-get autoremove -y && rm -rf /var/lib/{dpkg,cache,log}/ # keep /var/lib/apt

