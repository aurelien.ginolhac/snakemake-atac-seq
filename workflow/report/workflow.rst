[**ATAC-seq** peak-calling, QC and differential analysis pipeline](https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-atac-seq),
 derived from [Snakemake template](https://github.com/snakemake-workflows/chipseq) (itself port of the `nextflow pipeline <https://nf-co.re/chipseq>`).
