**DiffBind** is for Differential Binding Analysis of ChIP-Seq Peak Data available [here](https://bioconductor.org/packages/release/bioc/html/DiffBind.html)
