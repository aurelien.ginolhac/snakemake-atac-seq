The **Heatmap** comes from the [`deepTools suite`](https://deeptools.readthedocs.io/en/develop/).
Sample signals are produced by [`bamCoverage`](https://deeptools.readthedocs.io/en/develop/content/tools/bamCoverage.html) with RPGC normalization (the number of reads per bin / scaling factor for 1x average coverage. )
Then [`ComputeMatrix`](https://deeptools.readthedocs.io/en/develop/content/tools/computeMatrix.html) was run on all genes, +/- 3kb around the TSS.
Finally [`plotHeatmap`](https://deeptools.readthedocs.io/en/develop/content/tools/plotHeatmap.html) rendered the heatmap from the matrix.
