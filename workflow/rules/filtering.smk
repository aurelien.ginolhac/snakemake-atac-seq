
rule merge_se_pe:
    input:
        get_se_pe_branches_input
    output:
        "results/filtered/{group}_{replicate}.sorted.bam"
    params:
        ""
    log:
        "logs/filtered/{group}_{replicate}.sorted.log"
    shell:
        "ln -sr {input} {output}"
