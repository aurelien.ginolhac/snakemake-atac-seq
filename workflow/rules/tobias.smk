# Adapted from https://github.com/loosolab/TOBIAS_snakemake/blob/main/snakefiles/

# Get chromosomes available in fasta
rule get_fasta_chroms:
	input:
		"refs/genome.fasta"
	output:
		txt = "refs/chromsizes.txt",
		bed = "refs/chromsizes.bed"
	shell:
		"samtools faidx {input} --fai-idx {output.txt};"
		"awk '{{ print $1\"\t\"0\"\t\"$2 }}' {output.txt} > {output.bed}"

# Merge sample bam files to condition bam file
# if only one sample per condition, copy sample bam to merged bam file name for further processing

rule merge_bam:
	input:
		get_sample_group_list
	output:
		"results/merged/{group}.bam"
	threads: 8
	log: "logs/merged/{group}.log"
	params: lambda w, input: " ".join(input),
	shell:
		"""
		samtools merge -@ {threads} {output} {params}
		samtools index {output}
		"""


# Peak-calling
			
rule macs2:
	input:
		"bams/{sample}-{unit}.bam"
	output: 
		macs = "results/macs2/{group}/{sample}-{unit}_peaks.broadPeak"
	log: 
		"logs/macs2/{group}_{sample}-{unit}_peak_calling.log"
	params:
		"--name {sample}-{unit}",
		"--outdir results/macs2/{group}",
		"--gsize hs",
		"--nomodel --shift -100 --extsize 200 --broad"
	shell:
		"""
		macs2 callpeak -t {input} {params} &> {log}
		"""

# process peaks:
# 1. reduce to genomic location columns and sort
# 2. merge peaks per condition
# 3. remove blacklisted regions and add unique peak ids
rule process_peaks:
	input: 
		peaks = get_peak_group_list,
		blacklist = "refs/ENCFF356LFX.bed",
		whitelist = "refs/chromsizes.bed"
	output: 
		peaks = "results/macs2/{group}_union.bed"
	shell:
		"""
		cat {input.peaks} | cut -f1-3 | sort -k1,1 -k2,2n | bedtools merge -d 5 | 
		bedtools subtract -a - -b {input.blacklist} -A | 
		bedtools intersect -a - -b {input.whitelist} -wa | 
		awk '$1 !~ /[M]/' | 	
		awk '{{print $0\"\\t{wildcards.group}\"}}' > {output.peaks}
		"""

# Union peaks across all conditions
rule merge_condition_peaks:
	input: 
		expand("results/macs2/{group}_union.bed", group = samples.group.unique())
	output: 
		"results/macs2/all_merged.bed"
	message: 
		"Merging peaks across conditions"
	shell:
		"cat {input} | sort -k1,1 -k2,2n | bedtools merge -d 5 -c 4 -o distinct > {output}"

#Config for uropa annotation
rule uropa_config:
	input:
		bed = "results/macs2/all_merged.bed",
		gtf = "refs/genome.gtf"
	output:
		config = "results/peak_annotation/all_merged_annotated.config"
	run:
		import json
		config = {"queries":[
					{"feature":"gene", "feature.anchor":"start", "distance":[10000,1000], "filter_attribute":"gene_biotype", "attribute_values":"protein_coding", "name":"protein_coding_promoter"},
					{"feature":"gene", "distance":1, "filter_attribute":"gene_biotype", "attribute_values":"protein_coding", "internals":0.1, "name":"protein_coding_internal"},
					{"feature":"gene", "feature.anchor":"start", "distance":[10000,1000], "name":"any_promoter"},
					{"feature":"gene", "distance":1, "internals":0.1, "name":"any_internal"},
					{"feature":"gene", "distance":[50000, 50000], "name":"distal_enhancer"},
					],
				"show_attributes":["gene_biotype", "gene_id", "gene_name"],
				"priority":"True"
				}

		config["gtf"] = input.gtf
		config["bed"] = input.bed

		string_config = json.dumps(config, indent=4)

		config_file = open(output[0], "w")
		config_file.write(string_config)
		config_file.close()

# Peak annotation
# peaks per condition or across conditions, dependent on run info output
rule uropa:
	input: 
		config = "results/peak_annotation/all_merged_annotated.config"
	output:
		finalhits =  "results/peak_annotation/all_merged_annotated_finalhits.txt",
		finalhits_sub =  "results/peak_annotation/all_merged_annotated_finalhits_sub.txt",
		peaks =  "results/peak_annotation/all_merged_annotated.bed",
		header =  "results/peak_annotation/all_merged_annotated_header.txt"
	threads: 
		12
	log: 
		"logs/uropa.log"
	params:
		prefix =  "results/peak_annotation/all_merged_annotated"
	shell:
	    """
		uropa --input {input.config} --prefix {params.prefix} --threads {threads} &> {log}; 
		cut -f 1-4,7-13,16-19 {output.finalhits} > {output.finalhits_sub}; 
		head -n 1 {output.finalhits_sub} > {output.header};
		tail -n +2 {output.finalhits_sub} > {output.peaks}
		"""


#Correct reads for Tn5 sequence bias
rule atacorrect:
	input:
		bam = "results/merged/{group}.bam",
		peaks = "results/peak_annotation/all_merged_annotated.bed",
		genome = "refs/genome.fasta"
	output:
		uncorrected = "results/bias_correction/{group}_uncorrected.bw",
		bias = "results/bias_correction/{group}_bias.bw",
		expected = "results/bias_correction/{group}_expected.bw",
		corrected = "results/bias_correction/{group}_corrected.bw",
	params: 
		"--blacklist refs/ENCFF356LFX.bed",
		"--outdir results/bias_correction/",
		"--prefix " + "{group}"
	threads: 
		12
	log: 
		"logs/tobias/{group}_atacorrect.log"
	shell:
		"TOBIAS ATACorrect -b {input.bam} -g {input.genome} -p {input.peaks} --cores {threads} {params} &> {log}"

#Calculate footprint scores per condition
rule footprinting:
	input: 
		signal = "results/bias_correction/{group}_corrected.bw",
		regions = "results/peak_annotation/all_merged_annotated.bed"
	output: 
		footprints = "results/footprinting/{group}_footprints.bw"
	threads: 
		12
	log:
		"logs/tobias/{group}_footprinting.log"
	shell:
		"TOBIAS FootprintScores --signal {input.signal} --regions {input.regions} --output {output.footprints} --cores {threads} &> {log}"

# Estimate bound sites from scored file
# https://github.com/loosolab/TOBIAS/wiki/BINDetect

checkpoint bindetect:
	input: 
		motifs = "refs/JASPAR2024_CORE_vertebrates_non-redundant_pfms_jaspar.txt",
		footprints = get_contrast_pair,
		genome = "refs/genome.fasta",
		peaks = "results/peak_annotation/all_merged_annotated.bed",
		peak_header = "results/peak_annotation/all_merged_annotated_header.txt"
	output:
		folder = directory("results/TFBS/{contrast}"),
		txt = "results/TFBS/{contrast}/bindetect_results.txt",
		html = report("results/TFBS/{contrast}/bindetect_figures.pdf", category = "TOBIAS", caption = "../report/tobias.rst")
	threads: 12
	params:
		fps = lambda w, input: " ".join(input.footprints),
	log:
		"logs/tobias/bindetect_{contrast}.log"
	shell:
		"""
		TOBIAS BINDetect --motifs {input.motifs} --signals {params.fps} --genome {input.genome} \
		--peaks {input.peaks} --peak_header {input.peak_header} --cores {threads} --outdir {output.folder} &> {log}
		"""

# Visualization

#Heatmaps split in bound/unbound within conditions
