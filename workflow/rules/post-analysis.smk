
rule sort_genomecov:
    input:
        rules.genrich_callpeak.output.bedgraph
    output:
        "results/genrich/{sample}.sort.bg"
    log:
        "logs/sort_genomecov/{sample}.log"
    shell:
        "tail --lines=+3 {input} | cut -f 1-4 | sort -k1,1 -k2,2n > {output} 2> {log}"




rule bedGraphToBigWig:
    input:
        bedGraph = "results/genrich/{sample}.sort.bg",
        chromsizes="resources/ref/genome.chrom.sizes"
    output:
        "results/big_wig/{sample}.bigWig"
    log:
        "logs/big_wig/{sample}.log"
    params:
        ""
    wrapper:
        "0.64.0/bio/ucsc/bedGraphToBigWig"