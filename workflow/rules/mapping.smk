rule bowtie2_build:
    input:
        ref="refs/genome.fasta"
    output:
        multiext(
            "refs/genome",
            ".1.bt2",
            ".2.bt2",
            ".3.bt2",
            ".4.bt2",
            ".rev.1.bt2",
            ".rev.2.bt2",
        ),
    log:
        "logs/bowtie2_build/build.log",
    params:
        extra="",  # optional parameters
    threads: 8
    wrapper:
        "v1.31.1/bio/bowtie2/build"

rule bowtie2:
    input:
        sample=get_map_reads_input,
        idx=multiext(
            "refs/genome",
            ".1.bt2",
            ".2.bt2",
            ".3.bt2",
            ".4.bt2",
            ".rev.1.bt2",
            ".rev.2.bt2",
        )
    output:
        "results/mapped/{sample}-{unit}.bam",
    log:
        "logs/bowtie2/{sample}-{unit}.log",
    params:
        extra="",  # optional parameters
    threads: 8  # Use at least two threads
    shell:
         "bowtie2"
         " --threads {threads}"
         " -1 {input.sample[0]} -2 {input.sample[1]} "
         " -x refs/genome"
         " {params.extra} 2> {log} "
         "| samtools view -b --with-header "
         " - > {output}"
  


# took 3h30 to index

rule sort_bam:
    input:
        "results/mapped/{sample}-{unit}.bam",
    output:
        bam="bams/{sample}-{unit}.bam",
        bai="bams/{sample}-{unit}.bai"
    log:
        "logs/sort_bam/{sample}-{unit}.log"
    threads: 12 # to avoid running too many in parallel
    params:
        "CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT"
    shell:
       "picard SortSam"
        " I={input}"
        " O={output.bam}"
        " SORT_ORDER=coordinate"
        " {params}"
        " 2> {log}"

rule sort_bam_query:
    input:
        "results/mapped/{sample}-{unit}.bam",
    output:
        bam="bams_query/{sample}-{unit}.bam"
    log:
        "logs/sort_bam_query/{sample}-{unit}.log"
    threads: 12 # to avoid running too many in parallel
    params:
        "VALIDATION_STRINGENCY=LENIENT"
    shell:
       "picard SortSam"
        " I={input}"
        " O={output.bam}"
        " SORT_ORDER=queryname"
        " {params}"
        " 2> {log}"

rule bigwig:
    input:
        "bams/{sample}-{unit}.bam",
    output:
        "bigwig_norm/{sample}-{unit}_RPGC.bw",
    log:
        "logs/dp_bw/{sample}-{unit}.log"
    threads: 4
    message: "Effective Size is 2805636331 (GRC38 100 bp reads)"
    params:
        ""
    # RPGC (per bin) = number of reads per bin / scaling factor for 1x average coverage. 
    # None = the default and equivalent to not setting this option at all. 
    # This scaling factor, in turn, is determined from the sequencing depth: (total number of mapped reads * fragment length) / effective genome size.
    shell:
       "bamCoverage -b {input} -o {output} -p {threads} --effectiveGenomeSize 2805636331 --normalizeUsing RPGC"
        " 2> {log}"
