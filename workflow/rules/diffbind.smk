
rule diffbind_prep:
    input:
        peaks = get_peak_contrast_pair,
        bams = get_bam_contrast_pair
    output:
        csv = "results/diffbind/{contrast}/samplesheet.csv",
        rds = "results/diffbind/{contrast}/init.rds"
    log:
        "logs/diffbind/{contrast}/init.log"
    threads: 2
    params:
        group = get_group_contrast
    script:
        "../scripts/diffbind_init.R"

rule diffbind:
    input:
        rds = "results/diffbind/{contrast}/init.rds"
    output:
        rds = "results/diffbind/{contrast}/diffbind.rds"
    log:
        "logs/diffbind/{contrast}/diffbind.log"
    threads: 8
    script:
        "../scripts/diffbind.R"

rule diffbind_pca:
    input:
        rds = "results/diffbind/{contrast}/diffbind.rds"
    output:
        pdf = report("results/diffbind/{contrast}/pca.pdf", category = "DiffBind", caption = "../report/diffbind.rst")
    log:
        "logs/diffbind/{contrast}/pca.log"
    threads: 2
    script:
       "../scripts/diffbind_pca.R"


