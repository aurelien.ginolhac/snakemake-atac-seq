#
#rule genrich_callpeak_replicate:
#    input:
#        bam =  expand("results/merged/{sample.sample}.bam", sample = samples.itertuples()),
#        blacklist = "resources/ref/blacklist.bed"
#    output:
#        peaks = ["results/genrich/{gp}.narrowPeak".format(gp=group) for group in samples['group'].unique()],
#        bedgraph = ["results/genrich/{gp}.bg".format(gp=group) for group in samples['group'].unique()]
#    #log:
#    #    expand("logs/genrich/{group}.log", group = samples['group'].unique()),
#    params:
#        replicates = ["{join}".format(join=i) for i in get_sample_group_list()],
#        exclude = "chrM",
#        extra = "-a 500 -g 15 -l 15 -d 50"
#    shell:
#        # -r for duplicate removal
#        """
#        Genrich -t {params.replicates} -o {output.peaks} \
#        -k {output.bedgraph} -e {params.exclude} \
#        -E {input.blacklist} -r \
#        -m 30 -j {params.extra}
#        """


rule genrich_callpeak:
    input:
        #bam = get_merged_bams,
        bam =  "bams_query/{sample}-{unit}.bam",
        # from https://www.encodeproject.org/files/ENCFF356LFX/
        blacklist = "refs/ENCFF356LFX.bed"
    output:
        peaks = "results/genrich/{sample}-{unit}.narrowPeak",
        bedgraph = "results/genrich/{sample}-{unit}.bg"
    log:
        "logs/genrich/{sample}-{unit}.log"
    threads: 4
    params:
        exclude = "MT",
        extra = "-a 500 -g 15 -l 15 -d 50"
    shell:
        # -r for duplicate removal
        """
        Genrich -t {input.bam} -o {output.peaks} \
        -k {output.bedgraph} -e {params.exclude} \
        -E {input.blacklist} -r \
        -m 30 -j {params.extra}
        """

