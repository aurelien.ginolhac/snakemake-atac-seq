rule fastqc:
    input:
        get_individual_fastq
    output:
        html="results/qc/fastqc/{sample}.{unit}.{read}.html",
        zip="results/qc/fastqc/{sample}.{unit}.{read}_fastqc.zip"
    params:
        "--quiet"
    log:
        "logs/fastqc/{sample}.{unit}.{read}.log"
    threads: 4
    wrapper:
        "v1.7.0/bio/fastqc"

rule multiqc:
    input:
        get_multiqc_input
    output:
        report("results/qc/multiqc/multiqc.html", caption="../report/multiqc_report.rst", category="QC")
    log:
        "logs/multiqc.log"
    wrapper:
        "v1.7.0/bio/multiqc"

rule fastq_screen:
    input:
        get_individual_fastq
    output:
        txt="results/qc/fastq_screen/{sample}.{unit}.{read}.fastq_screen.txt",
        png="results/qc/fastq_screen/{sample}.{unit}.{read}.fastq_screen.png"
    log:
        "logs/fastq_screen/{sample}.{unit}.{read}.log"
    params:
        fastq_screen_config = {
            'database': {
                'human': {
                  'bowtie2': "{}/Human/Homo_sapiens.GRCh38".format(config["params"]["db_bowtie_path"])},
                'mouse': {
                    'bowtie2': "{}/Mouse/Mus_musculus.GRCm38".format(config["params"]["db_bowtie_path"])},
                'rat':{
                  'bowtie2': "{}/Rat/Rnor_6.0".format(config["params"]["db_bowtie_path"])},
                'drosophila':{
                  'bowtie2': "{}/Drosophila/BDGP6".format(config["params"]["db_bowtie_path"])},
                'worm':{
                  'bowtie2': "{}/Worm/Caenorhabditis_elegans.WBcel235".format(config["params"]["db_bowtie_path"])},
                'yeast':{
                  'bowtie2': "{}/Yeast/Saccharomyces_cerevisiae.R64-1-1".format(config["params"]["db_bowtie_path"])},
                'arabidopsis':{
                  'bowtie2': "{}/Arabidopsis/Arabidopsis_thaliana.TAIR10".format(config["params"]["db_bowtie_path"])},
                'ecoli':{
                  'bowtie2': "{}/E_coli/Ecoli".format(config["params"]["db_bowtie_path"])},
                'rRNA':{
                  'bowtie2': "{}/rRNA/GRCm38_rRNA".format(config["params"]["db_bowtie_path"])},
                'MT':{
                  'bowtie2': "{}/Mitochondria/mitochondria".format(config["params"]["db_bowtie_path"])},
                'PhiX':{
                  'bowtie2': "{}/PhiX/phi_plus_SNPs".format(config["params"]["db_bowtie_path"])},
                'Lambda':{
                  'bowtie2': "{}/Lambda/Lambda".format(config["params"]["db_bowtie_path"])},
                'vectors':{
                  'bowtie2': "{}/Vectors/Vectors".format(config["params"]["db_bowtie_path"])},
                'adapters':{
                  'bowtie2': "{}/Adapters/Contaminants".format(config["params"]["db_bowtie_path"])},
                'mycoplasma':{
                  'bowtie2': "{}/Mycoplasma/mycoplasma".format(config["params"]["db_bowtie_path"])}
                 },
                 'aligner_paths': {'bowtie2': "{}/bowtie2".format(config["params"]["bowtie_path"])}
                },
        subset=100000,
        aligner='bowtie2'
    threads: 6
    wrapper:
        "v1.7.0/bio/fastq_screen"

rule split_ref:
    input:
        "refs/genome.gtf"
    output:
        temp(directory("refs/bed_split"))
    shell:
        """
        awk 'BEGIN{{OFS="\t"}}{{if($3=="transcript"){{print $1,$4-1,$5,substr($14,2,15),$6,$7}}}}' {input} > refs/tmp.bed
        mkdir -p refs/bed_split
        split -l 10000 --additional-suffix=.bed -d refs/tmp.bed refs/bed_split/chunk
        rm -f refs/tmp.bed
        """

rule compute_matrix:
    input:
        folder = rules.split_ref.output,
        bw = expand("bigwig_norm/{unit.sample}-{unit.unit}_RPGC.bw", unit = units.itertuples())
    output:
       temp(directory("refs/matrix"))
    threads: 8
    log: "logs/compute_matrix.log"
    params:
		    bigwig = lambda w, input: " ".join(input.bw),
    shell:
        # checkpoints should replace the for loop here to parallelize this rule (right now takes 3h for 9 human samples)
        # https://snakemake.readthedocs.io/en/stable/snakefiles/rules.html#snakefiles-checkpoints
        """
        mkdir -p {output}
        for CHUNK in {input.folder}/chunk??.bed
        do echo "computeMatrix $CHUNK"
        computeMatrix reference-point --referencePoint TSS  -b 3000 -a 3000 -R $CHUNK \
        -S {params.bigwig} -p {threads} --skipZero --smartLabels \
        -o {output}/`basename $CHUNK .bed/`.gz &> {log}
        done
        """

rule merge_matrix:
    input:
        rules.compute_matrix.output
    output:
        temp("results/qc/matrix_TSS.gz")
    threads: 8
    log: "logs/merge_matrix.log"
    shell:
        """
        computeMatrixOperations rbind -m {input}/*.gz -o results/qc/matrix_TSS.gz &> {log}
        """

rule plot_heatmap:
    input:
        "results/qc/matrix_TSS.gz"
    output:
        report("results/qc/heatmap.png", caption="../report/heatmap.rst", category="QC")
    threads: 4
    shell:
        """
        plotHeatmap -m {input} -o {output} --colorMap viridis
        """

