from snakemake.utils import validate
import pandas as pd
import numpy as np
import os
from smart_open import open
import yaml

# this container defines the underlying OS for each job when using the workflow
# with --use-conda --use-singularity
# container: "docker://continuumio/miniconda3"

##### load config and sample sheets #####

validate(config, schema="../schemas/config.schema.yaml")

samples = pd.read_table(config["samples"], sep="\t", dtype = str).set_index("sample", drop=False)
samples.index.names = ["sample_id"]
validate(samples, schema="../schemas/samples.schema.yaml")

units = pd.read_table(
    config["units"], dtype=str, sep="\t").set_index(["sample", "unit"], drop=False)
units.index.names = ["sample_id", "unit_id"]
units.index = units.index.set_levels(
    [i.astype(str) for i in units.index.levels])  # enforce str in index
units = units.sort_index() # fix Performance warnings
validate(units, schema="../schemas/units.schema.yaml")

build = config["ref"]["build"]
chromosome = config["ref"]["chromosome"]

##### Check contrasts

for contrast in config["contrasts"]:
    assert len(contrast.split("_vs_")) == 2, "contrasts in config.yaml must in the form XX_vs_YY (provided: {})".format(contrast)
    for gp in contrast.split('_vs_'):
        assert gp in samples["group"].tolist(), "group in contrasts must be present in samples.tsv, group column ({} missing)".format(gp)


##### wildcard constraints #####

wildcard_constraints:
    sample="|".join(samples["sample"]),
    unit="|".join(units["unit"]),
    read="|".join(["0", "1", "2"])


####### helpers ###########

def is_single_end(sample, unit):
    fq2=pd.isnull(units.loc[(sample, unit), ["fq2"]].values.flatten().tolist())
    if len(fq2) > 1:
        assert len(np.unique(fq2)) == 1, "units per sample are expected to of same seq type (se/pe)"
    return np.unique(fq2)

def get_individual_fastq(wildcards):
    """Get individual raw FASTQ files from unit sheet, based on a read (end) wildcard"""
    if ( wildcards.read == "0" or wildcards.read == "1" ):
        return units.loc[ (wildcards.sample, wildcards.unit), "fq1" ]
    elif wildcards.read == "2":
        return units.loc[ (wildcards.sample, wildcards.unit), "fq2" ]

def get_fastq(wildcards):
    fastqs = units.loc[(wildcards.sample, wildcards.unit), ["fq1", "fq2"]].dropna()
    if len(fastqs) == 2:
        return {"fq1": fastqs.fq1, "fq2": fastqs.fq2}
    return {"fq1": fastqs.fq1}


def get_se_pe_branches_input(wildcards):
    if config["single_end"]:
        return "results/bamtools_filtered/{sample}.sorted.bam".format(sample=wildcards.sample)
    else:
        return "results/orph_rm_pe/{sample}.sorted.bam".format(sample=wildcards.sample)


def get_sample_group_list(wc):
    gp_samples = samples[samples['group'] == wc.group]["sample"].tolist()
    bams = expand("bams/{unit.sample}-{unit.unit}.bam", unit = units[units['sample'].isin(gp_samples)].itertuples())
    return bams

def get_peak_group_list(wc):
    gp_samples = samples[samples['group'] == wc.group]["sample"].tolist()
    peaks = expand("results/macs2/{group}/{unit.sample}-{unit.unit}_peaks.broadPeak", unit = units[units['sample'].isin(gp_samples)].itertuples(), group = wc.group)
    return peaks

def get_contrast_pair(wc):
    fps = expand("results/footprinting/{gp}_footprints.bw", gp = wc.contrast.split("_vs_"))
    return fps

def get_peak_contrast_pair(wc):
    group = wc.contrast.split("_vs_")
    peaks = []
    for gp in group:
        gp_samples = samples[samples['group'] == gp]["sample"].tolist()
        peaks.extend(
            expand("results/macs2/{group}/{unit.sample}-{unit.unit}_peaks.broadPeak", 
            unit = units[units['sample'].isin(gp_samples)].itertuples(),
            group = gp)
        )
    #peaks = expand("results/genrich/{unit.sample}-{unit.unit}.narrowPeak", unit = units[units['sample'].isin(gp_samples)].itertuples())
    return peaks

def get_group_contrast(wc):
    group = wc.contrast.split("_vs_")
    return samples[samples['group'].isin(group)]["group"].tolist()

def get_bam_contrast_pair(wc):
    group = wc.contrast.split("_vs_")
    gp_samples = samples[samples['group'].isin(group)]["sample"].tolist()
    bams = expand("bams/{unit.sample}-{unit.unit}.bam", unit = units[units['sample'].isin(gp_samples)].itertuples())
    return bams


def get_merged_bams(wildcards):
    return "results/merged/{sample}.bam".format(sample=wildcards.sample)

def get_map_reads_input(wildcards):
    if is_single_end(wildcards.sample, wildcards.unit):
        return "results/trimmed_se/{sample}-{unit}.fastq.gz"
    return ["results/trimmed_pe/{sample}-{unit}.1.fastq.gz", "results/trimmed_pe/{sample}-{unit}.2.fastq.gz"]

def get_read_group(wildcards):
    """Denote sample name and platform in read group."""
    return r"-R '@RG\tID:{sample}-{unit}\tSM:{sample}-{unit}\tPL:{platform}'".format(
        sample=wildcards.sample,
        unit=wildcards.unit,
        platform=units.loc[(wildcards.sample, wildcards.unit), "platform"])

def get_multiqc_input(wildcards):
    multiqc_input = []
    for (sample, unit) in units.index:
        reads = [ "1", "2" ]
        if is_single_end(sample, unit):
            reads = [ "0" ]
            multiqc_input.extend(expand (["logs/trimming/{sample}-{unit}.se.log"],
            sample = sample, unit = unit))
        else:
            multiqc_input.extend(expand (["logs/trimming/{sample}-{unit}.pe.log"],
            sample = sample, unit = unit))

        multiqc_input.extend(
            expand (
                [
                    "results/qc/fastqc/{sample}.{unit}.{reads}_fastqc.zip",
                    "results/qc/fastqc/{sample}.{unit}.{reads}.html",
                    "results/qc/fastq_screen/{sample}.{unit}.{reads}.fastq_screen.txt",
                    "results/qc/fastq_screen/{sample}.{unit}.{reads}.fastq_screen.png",
                    "logs/bowtie2/{sample}-{unit}.log"
                ],
                sample = sample,
                unit = unit,
                reads = reads
            )
        )

    return multiqc_input

def all_input(wildcards):

    wanted_input = []

    # QC with fastQC and multiQC
    wanted_input.extend([
        "results/qc/multiqc/multiqc.html",
        "results/qc/heatmap.png"
    ])

    # trimming reads
    for (sample, unit) in units.index:
        if is_single_end(sample, unit):
            wanted_input.extend(expand(
                    [
                        "results/trimmed_se/{sample}-{unit}.fastq.gz",
                        "results/trimmed_se/{sample}-{unit}.settings"
                    ],
                    sample = sample,
                    unit = unit
                )
            )
        else:
            wanted_input.extend(
                expand (
                    [
                        "results/trimmed_pe/{sample}-{unit}.1.fastq.gz",
                        "results/trimmed_pe/{sample}-{unit}.2.fastq.gz",
                        "results/trimmed_pe/{sample}-{unit}.settings"
                    ],
                    sample = sample,
                    unit = unit
                )
            )
    for (sample, unit) in units.index:
        wanted_input.extend(
            expand(
                [
                    "bigwig_norm/{sample}-{unit}_RPGC.bw",
                    "results/genrich/{sample}-{unit}.narrowPeak"
                ], 
                sample = sample, 
                unit = unit
            )
        )
    for group in samples.group.unique():
        wanted_input.extend(
            expand(
                [
                    "results/merged/{group}.bam",
                    "results/macs2/{group}_union.bed",
                    "results/footprinting/{group}_footprints.bw"
                ], 
                group = group
            )
        )
    for contrast in config["contrasts"]:
        wanted_input.extend(
            expand(
                ["results/TFBS/{contrast}/bindetect_results.txt",
                 "results/diffbind/{contrast}/pca.pdf",
                ], 
            contrast = contrast)
        )
    return wanted_input


