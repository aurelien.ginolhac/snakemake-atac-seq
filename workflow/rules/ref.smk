
rule get_genome:
    output:
        "refs/genome.fasta"
    params:
        species=config["ref"]["species"],
        datatype="dna",
        build=config["ref"]["build"],
        release=config["ref"]["release"]
    log:
        "logs/genome.log"
    wrapper:
        "v2.6.0/bio/reference/ensembl-sequence"

rule get_annotation:
    output:
        "refs/genome.gtf"
    params:
        species=config["ref"]["species"],
        build=config["ref"]["build"],
        release=config["ref"]["release"],
        fmt="gtf"
    log:
        "logs/annotation.log"
    wrapper:
        "v2.6.0/bio/reference/ensembl-annotation"




