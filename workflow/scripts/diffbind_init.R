log <- file(snakemake@log[[1]], open = "wt")
sink(log)
sink(log, type = "message")

suppressPackageStartupMessages(library(DiffBind))
suppressPackageStartupMessages(library(dplyr))
library(stringr)
library(fs)

pfiles <- snakemake@input[["peaks"]]
group <- snakemake@params[["group"]]

tibble::tibble(
  SampleID = path_file(pfiles) |> path_ext_remove(),
  Tissue = group,
  Factor = "ATAC",
  Condition = Tissue,
  Treatment = "ATAC",
  bamReads = snakemake@input[["bams"]],
  ControlID = NA,
  bamControl = NA,
  Peaks = pfiles,
  # so score column is 7
  PeakCaller = "sicer") |>
  mutate(Replicate = row_number(),
         .by = Tissue, .after = Treatment) -> sampleSheet

head(sampleSheet, 10)

write.csv(sampleSheet, file = snakemake@output[["csv"]], row.names = FALSE)

diffbind_init <- dba(sampleSheet = snakemake@output[["csv"]])
diffbind_init
saveRDS(diffbind_init, file = snakemake@output[["rds"]])
