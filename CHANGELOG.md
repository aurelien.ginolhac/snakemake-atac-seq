## Development version

- Use `macs2` broadPeak for `DiffBind` analysis too

## v0.1.0 2024-03-07

- `Genrich` works now for replicates (fix #1)
- Switch from `bwa` to `bowtie2`. `bwa` does not compile on gcc10
- Add `TOBIAS` differential binding analysis
- Add `DiffBind` differential binding analysis
- Add `deepTools` heatmap centered on TSS

## v0.0.2 2021-09-01

- First release (for ChIP-seq course)
