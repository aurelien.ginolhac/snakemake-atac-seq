# Snakemake workflow: ATAC-seq

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.4.0-brightgreen.svg)](https://snakemake.github.io)

This [workflow](https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-atac-seq) is derived from [Snakemake template](https://github.com/snakemake-workflows/chipseq) (itself port of the [nextflow chipseq pipeline](https://nf-co.re/chipseq)) and performs ChIP-seq peak-calling, QC and differential analysis.

## Overview

- Preprocessing

Reads are trimmed by [`AdapterRemoval`](https://github.com/MikkelSchubert/adapterremoval) and mapped against the genome by [`bowtie2`](https://github.com/BenLangmead/bowtie2).

- Two peak callers are used:

- [`Genrich`](https://github.com/jsh58/Genrich)
- [`macs2`](https://hbctraining.github.io/Intro-to-ChIPseq/lessons/05_peak_calling_macs.html) as recommended for `TOBIAS`

- Two tools are used for differential binding analysis (both using `macs2` broad peaks)

- [`DiffBind`](https://bioconductor.org/packages/DiffBind/), differential peaks
- [`TOBIAS`](https://github.com/loosolab/TOBIAS), differential footprinting of transcription factors

- Quality controls are performed by:
    + [`fastqc`](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) 
    + [`fastq Screen`](https://www.bioinformatics.babraham.ac.uk/projects/fastq_screen/)
    + [`multiqc`](https://multiqc.info/) will gather qc, trimming, alignment outputs and counting statistic for producing a final `html` report.


The different rules dependencies are depicted below:

![](https://i.ibb.co/1MbNqK6/rule.png)

Importantly, all software are available in a [docker image](https://hub.docker.com/repository/docker/ginolhac/snake-atac-seq). 
`Snakemake` will automatically fetch the image and convert it to a [Apptainer](https://apptainer.org/) image and cache it until the tag change.



## Install the ATAC-seq template

In the destination folder of your choice, otherwise create the folder such as:

```bash
mkdir snakemake-atac-seq
cd snakemake-atac-seq
``` 

and run the following commands:

```bash
VERSION="v0.1.0"
wget -qO- https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-atac-seq/-/archive/${VERSION}/snakemake-atac-seq-${VERSION}.tar.gz | tar xfz - --strip-components=1
```

this command will download, extract (without the root folder) the following files:

```
CHANGELOG.md
config/
Dockerfile
LICENSE
README.md
workflow/
```

you may want to delete the `Dockerfile`, `LICENSE`, `CHANGELOG.md` and `README.md` if you wish, 
they are not used by `snakemake` for runtime.

### Additional files

Human transcription factors from [`JASPAR`](https://jaspar.elixir.no/downloads/)

`refs/JASPAR2024_CORE_vertebrates_non-redundant_pfms_jaspar.txt`

Human blacklist from [ENCODE](https://www.encodeproject.org/files/ENCFF356LFX/)

`refs/ENCFF356LFX.bed`


## Configuration files

### Units

The supplied template describes a paired-end experiment.
So, after alignment of tabulated fields, it looks like this:

```
sample	 unit	                         fq1	                         fq2	platform
mDAN_D30_1	1	fastq/mDAN_D30_1_R1.fastq.gz	fastq/mDAN_D30_1_R2.fastq.gz	ILLUMINA
mDAN_D30_2	1	fastq/mDAN_D30_2_R1.fastq.gz	fastq/mDAN_D30_2_R2.fastq.gz	ILLUMINA
[...]
```


### Samples

A 2 columns tsv file. Such as:

```
sample	    group
mDAN_D30_1	mDAN_D30
mDAN_D30_2	mDAN_D30
smNPC_1	    smNPC
smNPC_2	    smNPC
```


### Config in `config/config.yaml`

Adjust the `ref` variables to fit your organism. Using the Ensembl notation.

Trimming is standard for ATAC-seq but check out the adapter sequences.
Currently the default is `--adapter1 CTGTCTCTTATACACATCT --adapter2 CTGTCTCTTATACACATCT`

Contrasts for differential binding are set in the `config/config.yaml` file such as:

`group1_vs_group2` where `group1` and `group2` must exist in `samples.tsv` 'group' column

More than one contrast can be written as follows:

``` yaml
contrasts:
  - group2_vs_group1
  - group3_vs_group1
```

## Running the workflow

Most probably it runs on a High-Performance Computer.

A `dry-run` to start with is recommended:

```bash
snakemake --use-singularity --singularity-args "-B /scratch/users/aginolhac:/mnt/data/" -j 12 -n
```

- the `--use-singularity` option indicates `snakemake` to fetch and use the docker image linked in the `Snakefile`.
- the `--singularity-args` bind my `scratch` to the singularity container so `fastq_screen` finds the different pre-indexed references.
- we booked *12* cores, tell `snakemake` about it with `-j`, finally `-n` is the `dry-run flag.

If all goes well, which is: no red text appears, you can generate the final report using:

```bash
snakemake --report
```
